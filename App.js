import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Platform,
  ScrollView,
  Button,
} from 'react-native';
import OneSignal from 'react-native-onesignal';

class App extends Component {
  constructor(properties) {
    super(properties);
}

  componentWillMount() {
    OneSignal.setLogLevel(7, 0);

    OneSignal.init("bfda89ef-edf6-45fb-ad5a-7c107327b006");
    this.setState({ 
      initialOpenFromPush : "Did NOT open from push",
      activityWidth : 0,
      width: 0,
      activityMargin: 0,
      buttonColor : Platform.OS == "ios" ? "#ffffff" : "#d45653",
      jsonDebugText : ""
  });
  OneSignal.setLocationShared(true);

    
    OneSignal.inFocusDisplaying(2);
  }

  componentDidMount() {
    this.onReceived = this.onReceived.bind(this);
    this.onOpened = this.onOpened.bind(this);
    this.onIds = this.onIds.bind(this);
    

    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);
}

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
  }
 
  onReceived(notification) {
    console.log("Notification received: ", notification);
}

onOpened(openResult) {
  console.log('Message: ', openResult.notification.payload.body);
  console.log('Data: ', openResult.notification.payload.additionalData);
  console.log('isActive: ', openResult.notification.isAppInFocus);
  console.log('openResult: ', openResult);
}

onIds(device) {
console.log('Device info: ', device);
}

  render() {
    return (
      <ScrollView style={styles.scrollView}>
                <View style={styles.container}>
     
                    <Text style={styles.welcome}>
                        Welcome to React Native!
                    </Text>
                    <Text style={styles.instructions}>
                        To get started, edit index.android.js
                    </Text>
                    <Text style={styles.instructions}>
                        Double tap R on your keyboard to reload,{'\n'}
                        Shake or press menu button for dev menu
                    </Text>
                    <View style={{flexDirection: 'row', overflow: 'hidden'}}>
                        <View style={styles.buttonContainer}>
                            <Button style={styles.button}
                                onPress={() => {
                                    OneSignal.getTags((tags) => {
                                        console.log("Did get tags: ", tags);

                                        this.setState({jsonDebugText : JSON.stringify(tags, null, 2)});
                                    });
                                }}
                                title="Get Tags"
                                color={this.state.buttonColor}
                            />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button style={styles.button}
                                onPress={() => {
                                    console.log("Sending tags");

                                    OneSignal.sendTags({"test_property_1" : "test_value_1", "test_property_2" : "test_value_2"});
                                }}
                                title="Send Tags"
                                color={this.state.buttonColor}
                            />
                        </View>
                    </View>

                    <View style={styles.buttonContainer}>
                        <Button style={styles.button}
                            onPress={() => {
                                OneSignal.getPermissionSubscriptionState((subscriptionState) => {
                                    this.setState({jsonDebugText : JSON.stringify(subscriptionState, null, 2)});
                                });
                            }}
                            title="Print Subscription State"
                            color={this.state.buttonColor}
                        />
                    </View>
                    <Text style={styles.jsonDebugLabelText}>
                        {this.state.jsonDebugText}
                    </Text>
                </View>
            </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

export default App;